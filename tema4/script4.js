var table = document.getElementById("ID");
var fields = [];
var header = [];
var clickIndex = 0;
if (table != null) {
    for (var i = 1; i < table.rows.length; i++) {

        fields.push([]);
        for (var j = 0; j < table.rows[i].cells.length; j++) {

            fields[i - 1].push(table.rows[i].cells[j].innerHTML);
        }
    }
    for (var j = 0; j < table.rows[0].cells.length; j++) {
        header.push(table.rows[0].cells[j].innerHTML);
    }
    for (var j = 0; j < table.rows[0].cells.length; j++)
        table.rows[0].cells[j].onclick = function() {
            tableText(this, j);
        };
}

function tableText(tableCell, column) {
    for (var i = 0; i < header.length; i++) {
        if (header[i] == tableCell.innerHTML) {
            column = i;
        }
    }
    clickIndex = (clickIndex + 1) % 1999999973;
    if (clickIndex % 2 == 1) {
        fields = fields.sort(function(a, b) {

            if (a[column] < b[column]) {
                return -1;
            }
            if (a[column] > b[column]) {
                return 1;
            }
            return 0;
        });

    } else {
        fields = fields.sort(function(a, b) {

            if (a[column] < b[column]) {
                return 1;
            }
            if (a[column] > b[column]) {
                return -1;
            }
            return 0;
        });
    }
    for (var i = 0; i < table.rows.length; i++) {
        table.rows[i + 1].cells[column].innerHTML = fields[i][column];
    }
    console.log(fields);
}