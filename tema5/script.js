$(document).ready(function() {

    var firstClick = 0,
        secondClick = 0,
        thirdClick = 0,
        forthClick = 0;

    function sortTableFirst(table, order) {
        var asc = order,
            tbody = table.find('#tableBody');

        tbody.find('tr').sort(function(a, b) {
            if (asc == "asc") {
                return $('td:nth-child(1)', a).text().localeCompare($('td:first', b).text());
            } else {
                return $('td:nth-child(1)', b).text().localeCompare($('td:first', a).text());
            }
        }).appendTo(tbody);
    }

    function sortTableSecond(table, order) {
        var asc = order,
            tbody = table.find('#tableBody');

        tbody.find('tr').sort(function(a, b) {
            if (asc == "asc") {
                return $('td:nth-child(2)', a).text().localeCompare($('td:nth-child(2)', b).text());
            } else {
                return $('td:nth-child(2)', b).text().localeCompare($('td:nth-child(2)', a).text());
            }
        }).appendTo(tbody);
    }

    function sortTableThird(table, order) {
        var asc = order,
            tbody = table.find('#tableBody');

        tbody.find('tr').sort(function(a, b) {
            if (asc == "asc") {
                return $('td:nth-child(3)', a).text().localeCompare($('td:nth-child(3)', b).text());
            } else {
                return $('td:nth-child(3)', b).text().localeCompare($('td:nth-child(3)', a).text());
            }
        }).appendTo(tbody);
    }

    function sortTableForth(table, order) {
        var asc = order,
            tbody = table.find('#tableBody');

        tbody.find('tr').sort(function(a, b) {
            if (asc == "asc") {
                return $('td:nth-child(4)', a).text().localeCompare($('td:nth-child(4)', b).text());
            } else {
                return $('td:nth-child(4)', b).text().localeCompare($('td:nth-child(4)', a).text());
            }
        }).appendTo(tbody);
    }
    // console.log($('#tableBody').html());
    $('#header1').click(function() {
        firstClick = (firstClick + 1) % 1999999973;
        if (firstClick % 2 == 1) {
            sortTableFirst($('#table'), 'asc');
        } else {
            sortTableFirst($('#table'), 'desc');
        }
    });
    $('#header2').click(function() {
        secondClick = (secondClick + 1) % 1999999973;
        if (secondClick % 2 == 1) {
            sortTableSecond($('#table'), 'asc');
        } else {
            sortTableSecond($('#table'), 'desc');
        }
    });
    $('#header3').click(function() {
        thirdClick = (thirdClick + 1) % 1999999973;
        if (thirdClick % 2 == 1) {
            sortTableThird($('#table'), 'asc');
        } else {
            sortTableThird($('#table'), 'desc');
        }
    });
    $('#header4').click(function() {
        forthClick = (forthClick + 1) % 1999999973;
        if (forthClick % 2 == 1) {
            sortTableForth($('#table'), 'asc');
        } else {
            sortTableForth($('#table'), 'desc');
        }
    });
    $('#footer1').click(function() {
        moveColumn($('#table'), 1, 2);
    });
    $('#footer2').click(function() {
        moveColumn($('#table'), 2, 3);
    });
    $('#footer3').click(function() {
        moveColumn($('#table'), 3, 4);
    });
    $('#footer4').click(function() {

        moveColumn($('#table'), 4, 1);
    });



    function moveColumn(table, from, to) {
        var rows = table.find("tr");

        var cols;
        rows.each(function() {
            cols = jQuery(this).children('th, td');
            // console.log(cols[from - 1].textContent)
            var aux = cols[from - 1].textContent;
            cols[from - 1].textContent = cols[to - 1].textContent;
            cols[to - 1].textContent = aux;
        });
    }
});